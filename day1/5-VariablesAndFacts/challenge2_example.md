# Challenge 2

You may have noticed there isn't a lot of data provided by the get_facts module, and perhaps thought, "there is a lot more information in my network device than that - I wonder how I get the rest of it?"

In the first exercise we looked at collections, and how to use the `ansible-navigator` command to get more information about the modules and plugins available in them.  We just used the cisco.ios.ios_facts module without any arguments to get facts about an endpoint; can you use the `ansible-navigator doc` command to get more information about the module, and to learn how to get more information about your network device?

The challenge is to use the `ios_facts` module to print out information about the interfaces, l2_interfaces, l3_interfaces, vlans, and ntp.  Review the information in the ansible-navigator TUI.


## Step 1

Use the `ansible-navigator` tool to review the `ios_facts` module to see how you can gather additional information about network endpoints (specifically Cisco IOS devices in this case).

```bash
[student@ansible-1 ~]$ ansible-navigator doc cisco.ios.ios_facts
```

In the text based user interface, review the documentation for the ios_facts module.  Look for an option that would enable you to gather additional network resources such as those specified in the challenge above.

Create a playbook using either the terminal or by creating a new file in Visual Studio Code that will gather facts from the network device and then print out those facts.

```bash
[student@ansible-1 playbooks]$ vi ~/student-repo/playbooks/gather_more_facts.yml
```
```yaml
---
- name: MORE FACTS!
  hosts: cisco
  gather_facts: false

  tasks:
  - name: Gather more facts from Cisco devices
    cisco.ios.ios_facts:
      gather_network_resources:
      - interfaces
      - l2_interfaces
      - l3_interfaces
      - vlans
      - ntp

  - name: Print out Cisco device facts
    ansible.builtin.debug:
      var: ansible_network_resources

...
```


The example can be found [here](#./gather_more_facts.yml)

[Click here](./README.md) to return to the Ansible Network Automation Workshop Variables and Facts Exercise